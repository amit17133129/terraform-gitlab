terraform {
  backend "s3" {
    bucket = "terraform-xxxxx-backend"
    key    = "terraform/dev/terraform.tfsatte"
    region = "us-east-1"
  }
}